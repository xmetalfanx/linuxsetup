#Codecs for openSUSE Tumbleweed with KDE

##Extra Multimedia Codecs for KDE

##An easy way to install a selection of popular multimedia codecs.


### Recommended Repositories   

  ####Packman Repository
    #####Packman Software Repository
    #####Packman build software packages to enable users to easily install and remove software on Linux. More specifically, they do so for software that is not shipped as part of distributions or that are shipped as an outdated version.

    <url>http://ftp.gwdg.de/pub/linux/packman/suse/openSUSE_Tumbleweed/</url>

  ####libdvdcss repository
    #####Repository with libdvdcss
    #####Encrypted DVD support</description>

   <url>http://opensuse-guide.org/repo/openSUSE_Tumbleweed/</url>


    ## Software
      ####libdvdcss2
       #####Select this to be able to play Encrypted DVDs.
       #####A simple library designed for accessing DVDs without having to bother about the decryption. Install this to be able to play Encrypted DVDs.

      #### vlc-codecs
        ##### Codecs for VideoLan Client (VLC).
        ####This Package enhances the functionality of the VLC VideoLAN Client with Codecs that are not available in a stock openSUSE distribution.

      ####k3b-codecs
        #####Extra Multimedia Codecs for K3b
        #####This package extends the opensuse package with some codecs. K3b is a CD burning application that supports Ogg Vorbis, MP3 audio files, DVD burning, CDDB, and much more.</description>

      ####ffmpeg
        #####Hyper fast MPEG1/MPEG4/H263/RV and AC3/MPEG audio encoder
        #####Hyper fast MPEG1/MPEG4/H263/RV and AC3/MPEG audio encoder.</description>


    ####lame
        #####LAME adds support for encoding audio data into the MP3 format
        #####LAME is an educational tool to be used for learning about MP3 encoding.  The goal of the LAME project is to use the open source model to improve the psycho acoustics, noise shaping and speed of MP3.  Another goal of the LAME project is to use these improvements for the basis of a patent free audio compression codec for the GNU project.

      ####gstreamer-plugins-libav
          #####GStreamer libav plug-in
          #####GStreamer libav plug-in contains one plugin with a set of elements using the libav library code. It contains most popular decoders as well as very fast colorspace conversion elements.</description>

      ####gstreamer-plugins-bad
          #####GStreamer Bad Plug-ins
          #####GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared to the rest. They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use.

      ####gstreamer-plugins-ugly
          #####GStreamer Ugly Plug-ins
          #####GStreamer Ugly Plug-ins is a set of plug-ins that have good quality and correct functionality, but distributing them might pose problems. The license on either the plug-ins or the supporting libraries might not be how we'd like. The code might be widely known to present patent problems.

      ####gstreamer-plugins-ugly-orig-addon
          #####GStreamer Ugly Plug-ins
          #####GStreamer Ugly Plug-ins is a set of plug-ins that have good quality and correct functionality, but distributing them might pose problems. The license on either the plug-ins or the supporting libraries might not be how we'd like. The code might be widely known to present patent problems.

     ####gstreamer-plugins-good
          #####GStreamer Streaming-Media Framework Plug-Ins
          #####GStreamer is a streaming media framework based on graphs of filters that operate on media data. Applications using this library can do anything media-related, from real-time sound processing to playing videos. Its plug-in-based architecture means that new data types or processing capabilities can be added simply by installing new plug-ins.
