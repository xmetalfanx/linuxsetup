
# Software Chart

## Are the packages located in a distro's DEFAULT repos?  IF NOT where are they related to the distro

#### This is not taking into account things,  like snaps and flatpaks ... which i SHOULD

#### Arch, Fedora, and Solus related - Combined to save space

| Program Name     | Arch    | Fedora 26    | Fedora 27    | Solus       |
| ---------------- | ------- | ------------ | ------------ | ----------- |
| Deadbeef         | Default | NOT Default  | NOT Default  | Default     |
| Filezilla        | Default | Default      | Default      | Default     |
| Smplayer         | Default | NOT Default  | NOT Default  | Default     |
| git              | Default |              |              | Default     |
| bleachbit        | Default | Default      | Default      | Default     |
| Makemkv          | AUR     | NOT Default  | NOT Default  | Default     |
| Pinta            | Default | Default      | Default      | Default     |
| Atom             | Default | RPM Download | RPM Download | Default     |
| Filebot          | AUR     | NOT Default  | NOT Default  | Snap        |
| Sublime Text     |         | NOT Default  | NOT Default  | Third-Party |
| CatFish    *     |         | Default      | Default      | Default     |
| Sound Juicer *   |         | Default      | Default      | Default     |
| Sound Converter* |         | Default      | Default      | Default     |
| Variety          |         |              |              | Default     |
| TMM              | TB+Run  | TB+Run       | TB+Run       | TB+Run      |

Fedora Message:
* = I do not see a listing for Fedora 27, though using logic if they are in the 25 and 26 repos, they "should" be in the Fedora 27 Repos

"Not Default" in Fedora will probably change as I look at RPM Fusion repos ... the status for those Not Default lists should switch to the free or nonfree RPM Fusion repos if the app is found there.

---

#### OpenSuse Related

| Program Name    | Leap 42.2 | Leap 42.3 | Tumbleweed |
| --------------- | --------- | --------- | ---------- |
| Deadbeef        |           |           |            |
| Filezilla       |           |           |            |
| Smplayer        |           |           |            |
| git             |           |           |            |
| bleachbit       |           |           |            |
| Pinta           |           |           |            |
| Atom            |           |           |            |
| Filebot         |           |           |            |
| Sublime Text    |           |           |            |
| CatFish         |           |           |            |
| Sound Juicer    |           |           |            |
| Sound Converter |           |           |            |
| Variety         |           |           |            |
| TMM             | TB+Run    | TB+Run    | TB+Run     |

#### Ubuntu Base Related

| Program Name    | Ubuntu 16.04                 | Ubuntu 17.10   | Ubuntu 17.10   |
| --------------- | ---------------------------- | -------------- | -------------- |
| Deadbeef        | DEB                          | DEB            | DEB            |
| Filezilla       | Default                      | Default        | Default        |
| Smplayer        | PPA for newest               | PPA for newest | PPA for newest |
| git             | Default                      | Default        | Default        |
| bleachbit       | Default                      | Default        | Default        |
| Makemkv         | PPA                          | PPA            | PPA            |
| Pinta           | Default                      | Default        | Default        |
| Atom            | DEB                          | DEB            | DEB            |
| Filebot         | DEB or Snap                  | DEB or Snap    | DEB or Snap    |
| Sublime Text    | Third Party                  | Third Party    | Third Party    |
| CatFish         | Default                      | Default        | Default        |
| Sound Juicer    | Default                      | Default        | Default        |
| Sound Converter | Default                      | Default        | Default        |
| Variety         | Probably a PPA - Not Default | Default        | Default        |
| TMM             | TB+Run                       | TB+Run         | TB+Run         |

---
TMM = Tiny Media Manager ... shortened in the chart for the Markdown Syntax

TB+Run = Tarball (download) + just run the main .sh file

* There may be some changes coming in the future with Solus and the "third party section" ... WHEN? ... I am not sure
