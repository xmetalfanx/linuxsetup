
## Main Ubuntu Table

| Menu Tasks                 |     | Ubuntu 16.04 LTS | Ubuntu 18.04 LTS | Ubuntu 19.04 * |
| -------------------------- | --- | ---------------- | ---------------- | -------------- |
| Update System              |     | Not Tested       | Not Tested       | Pass           |
| Install Codecs             |     | Not Tested       | Not Tested       | Not Coded/Tested     |


* Lubuntu 19.04 tested
* 
---

## ElementaryOS Table

| Menu Tasks                                                     | Test Results |
| -------------------------------------------------------------- | ------------ |
| Installing PPA Support, Gnome Tweak Tools, GDebi, and Synaptic | Not Tested   |
| (not included yet) Install elementaryTweakTool                 | Not Tested   |

---
## Theming Menu 
- for theming i am focusing testing on the official flavors vs "theming on Linux Lite" (like Xubuntu anyway), or Linux Mint 

| Menu Items           |      | Ubuntu 16.04 | Ubuntu 18.04 | Ubuntu 19.04* |
| :------------------- | :--- | :----------- | :----------- | :------------ |
| Install Adapta Theme |      | Not Tested   | Not Tested   | Fail          |
| Install Arc Theme    |      | Not Tested   | Not Tested   | Fail          |
| Install Mint-Y Theme |      | Not Tested   | Not Tested   | Fail          |
| Install Numix Theme  |      | Not Tested   | Not Tested   | Pass          |
| Install Plata Theme  |      | Not Tested   | Not Tested   |               |
| Install misc Icons   |      |              |              |               |
| Install misc Themes  |      |              |              |               |
| **All Listed Themes  |      | Not Tested   | Not Tested   |               |
|                      |      |              |              |               |

*Lubuntu 19.04 tested
** in the future put miscIcons and miscThemes here too

---
## Software Menu

| Menu Task                |      | Ubuntu 16.04 | Ubuntu 18.04 | Ubuntu 19.04 |
| :----------------------- | :--- | :----------- | :----------- | :----------- |
| Add Common Software      |      | Not Tested   | Not Tested   | Pass         |
| Etcher                   |      | Not Tested   | Not Tested   | Not Tested   |
| Wine and/or PlayonLinux  |      | Not Tested   | Not Tested   | Not Tested   |
| Build Essential Packages |      | Not Tested   | Not Tested   | Pass         |


## Web Browser Menu 

| Menu Tasks    |     | Ubuntu 16.04 LTS | Ubuntu 18.04 LTS | Ubuntu 19.04 * |
| ------------- | --- | ---------------- | ---------------- | -------------- |
| Google Chrome |     | Not Tested       | Not Tested       | Fail           |
| Chromium      |     | Not Tested       | Not Tested       | Pass           |
| Firefox       |     | Not Tested       | Not Tested       | Pass           |
| Vivaldi       |     | Not Tested       | Not Tested       | Fail           |


## Multimedia Apps

| Menu Tasks            |     | Ubuntu 16.04 LTS | Ubuntu 18.04 LTS | Ubuntu 19.04 |
| --------------------- | --- | ---------------- | ---------------- | ------------ |
| Tiny Media Manager    |     | Not Tested       | Not Tested       | Not Tested   |
| makeMKV               |     | Not Tested       | Not Tested       | Not Tested   |
| DeadBeef Media Player |     | Not Tested       | Not Tested       | Not Tested   |
| XnViewMP              |     | Not Tested       | Not Tested       | Not Tested   |


## Productivity/Office Apps 

| Menu Tasks  |     | Ubuntu 16.04 LTS | Ubuntu 18.04 LTS | Ubuntu 19.04 |
| ----------- | --- | ---------------- | ---------------- | ------------ |
| Atom Editor |     | Not Tested       | Not Tested       | Not Tested   |
| vscode      |     | Not Tested       | Not Tested       | Not Tested   |




---
N/A = Not Applicable

Testing these on all the Ubuntu flavors and variants (Linux Lite, ElementaryOS, ..etc) will be a problem for me... where it says "ubuntu" the test IS FOR Ubuntu
