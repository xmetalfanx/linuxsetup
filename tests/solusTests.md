## Main Functions 
| Menu Tasks    |      | Solus 4.0 |
| :------------ | :--- | :-------- |
| Update System |      | Pass      |


## Solus Submenu 

| Menu Item                                                         | Solus 4.0 |      |
| :---------------------------------------------------------------- | :-------- | :--- |
| Switch from Stable to Unstable Branch                             |           |      |
| Switch from Unstable Branch to Stable Branch                      |           |      |
| Add Unstable Repo (Doesn't do anything to currently used Branch ) |           |      |
| Disable Unstable Repo (only)                                      |           |      |

?? How do I handle the different branches (so to speak) ? 

## Software Submenu 

| Menu Task                |      | Solus 4.0  |
| :----------------------- | :--- | :--------- |
| Add Common Software      |      | Pass       |
| Etcher                   |      | Pass*      |
| Wine and/or PlayonLinux  |      | Not Tested |
| Build Essential Packages |      | Pass       |

* Downloads but you need to "chmod +x" the appimage

## Web Browser Menu 

| Menu Tasks    |     | Solus 4.0    |
| ------------- | --- | ------------ |
| Google Chrome |     | Pass         |
| Chromium      |     | Not in Repos |
| Firefox       |     | Pass         |
| Vivaldi       |     | Pass         |


## Multimedia Apps

| Menu Task             |      | Solus 4.0  |
| :-------------------- | :--- | :--------- |
| Tiny Media Manager    |      | Pass       |
| makeMKV               |      | Pass       |
| DeadBeef Media Player |      | Pass       |
| XnViewMP              |      | Not Tested |


## Productivity/Office Apps 

| Menu Task   |      | Solus 4.0 |
| :---------- | :--- | :-------- |
| Atom Editor |      | Pass **   |
| vscode      |      | Pass      |

**Seems to have extra output even though it works ... look into that 

## Theming Menu 

| Menu Items           |      | Solus 4.0                          |
| :------------------- | :--- | :--------------------------------- |
| Install Adapta Theme |      | Pass                               |
| Install Arc Theme    |      | Pass                               |
| Install Mint-Y Theme |      | Not Tested/Coded                   |
| Install Numix Theme  |      | Pass                               |
| Install Plata Theme  |      | Plata is Solus 4.0's default theme |
| **All Listed Themes  |      | Not Tested                         |
|                      |      |                                    |
| miscIcons            |      | Pass                               |
| miscThemes           |      | Pass                               |


---
NOTE: some of the "fails" may indicated that I just haven't coded it for Solus yet ... I haven't gone back with each test/task i am doing to check 