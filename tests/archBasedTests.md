
## Main Arch Tasks 

| Menu Tasks                                                |      | Arch | Manjaro    |
| :-------------------------------------------------------- | :--- | :--- | :--------- |
| Update System                                             |      | Fail | Not Tested |
| Install Codecs                                            |      | Pass | Not Tested |
| Add Sound Support (different from just installing codecs) |      | Pass | Not Tested |
| Add Archive Support                                       |      | Fail | Not Tested |

---
## Add AUR Support 
| Menu Task |      | Arch       | Manjaro    |
| :-------- | :--- | :--------- | :--------- |
| trizen    |      | Not Tested | Not Tested |
| yay       |      | Fail       | Not Tested |


--- 
### Browser Installer 

| Menu Items    |      | Arch       | Manjaro |
| :------------ | :--- | :--------- | :------ |
| Google Chrome |      | Not Tested |         |
| Chromium      |      | Pass       |         |
| Firefox       |      | Pass       |         |
| Vivaldi       |      | Not Tested |         |


| Menu Items               |      | Arch       | Manjaro    |
| :----------------------- | :--- | :--------- | ---------- |
| Install Adapta Theme     |      | Pass       | Not Tested |
| Install Arc Theme        |      | Pass       | Not Tested |
| Install Mint-Y Theme &&& |      | Not Tested | Not Tested |
| Install Numix Theme      |      | Not Tested | Not Tested |
| Install Plata Theme &&&  |      | Not Tested | Not Tested |
| Install Misc Themes      |      | Pass       | Not Tested |
| Install Misc Icons       |      | Pass       | Not Tested |
| All Listed Themes        |      | Pass | Not Tested |


## Productivity/Office Apps 

| Menu Task   |      | Arch   | Manjaro    |
| :---------- | :--- | :----- | :--------- |
| Atom Editor |      | Pass** | Not Tested |
| vscode      |      | Pass   | Not Tested |

**Passes but has some artifacts in output ... look into this 
    - Download is fine 
    - Install works 
      - issue is during "updating desktop file MIME" ... maybe not related to my scripts 


## Multimedia Apps
| Menu Task             |      | Arch  | Manjaro    |
| :-------------------- | :--- | :---- | :--------- |
| Tiny Media Manager    |      | Pass  | Not Tested |
| makeMKV               |      | Pass  | Not Tested |
| DeadBeef Media Player |      | Pass  | Not Tested |
| XnViewMP              |      | Fail* | Not Tested |


# Software Menus
## Install/Update for the specific apps

| Menu Task                |      | Arch       | Manjaro    |
| :----------------------- | :--- | :--------- | :--------- |
| Add Common Software      |      | Pass       | Not Tested |
| Etcher                   |      | Not Tested | Not Tested |
| Wine and/or PlayonLinux  |      | Not Tested | Not Tested |
| Build Essential Packages |      | Pass       | Not Tested |


&&& = Needs the AUR 