# Xmetal's Usual tasks

## 1 - Optimize Repos

- Fedora: search for fastest mirror 
- Manjaro Rank Mirrors 

- not sure why i dont have this already done but the same SORT OF thing I have for Manjaro, will have an Arch function 

## 2 - Update System

## 3 - Install third party repos

- OpenSuse - Pacman Repos
- Fedora - RPM Fusion

## 4 - (if needed) Install Codecs

## 5 - Install common software

- list update: April 2, 2019
- Software: filezilla picard easytag deluge hexchat firefox mpv smplayer variety dconf-editor gparted ffmpeg youtube-dl inxi mediainfo bleachbit fish git menulibre

## 6 - Install fish shell + OhMyFish + some extras

- extras: bobthefish theme
