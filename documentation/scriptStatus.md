# Status of Scripts

## Arch Scripts

- Update System

- Optimize Mirrors

- Add Sound Support

- Add Archive Support

## Fedora Scripts

- Update Fedora

- Add RPMFusion and/or UnitedRPM (Submenu)

- Add Fedy

- Perform common Fedy Tasks (without having to install Fedy... per-say)

- Add common Software

- Install The Arc Theme

- All Tasks

- Return to Main menu

- Quit to Prompt

## OpenSuse Scripts

- Update OpenSuse

- Add Pacman Repos (Leap or TW)

- Setup Multimedia Codecs

- Install Google Chrome

- Install Arc Theme (again this is going to be one function)

- Install Extra Software (moving to Universal function)

- TODO: Add All functions option

- Return to Main menu

- Quit to Prompt

## Solus Scripts

- Update Solus via commandline (vs software center program)

- Switch from Stable to Unstable Branch

- Switch from Unstable to Stable Branch

- I MAY BE REMOVING ALOT OF THESE THAT I HAD BELOW

## Ubuntu Scripts

- Update System - Status -

- Add Theme and Icon PPA - Status -
  p better support for sorting between distro branches is needed
- Install common/well rated themese and/or Icon Packs

- Add Various Software (Universal Function again)

- Budgie Remix / Ubuntu Budgie Theming addition

- Status - Function can not be found

- ElementaryOS function - Adds PPA support, Synaptic and Gnome Tweak

- Ubuntu Mate specific - Add the Ubuntu Mate (official) Development PPA

- TODO: Add All functions option

- Return to Main menu

- Quit to Prompt