# Xmetal Linux Script


## Main Menu 
1.   Update your System
    - Updates the system via the command-line
  
2.  Optmize Repos/Mirrors
    - Works on Fedora with DNF
    - Works on Manjaro
    - [Coming Soon] Will work on Arch
  
3. Add Third Party Repos
   - Universal function is run here
   - Fedora - Adds both RPM Fusion Repos 
   - OpenSuse - Adds Pacman Repo(s)

4.  Adds Codecs
    - Adds needed Codecs on Arch, Fedora, and OpenSuse 

5.  [Menu] Arch-based
6.  [Menu] Fedora
7.  [Menu] OpenSUSE
8.  [Menu] Ubuntu based
9.  [Menu] Solus


s.   [Menu] Install Software
      - Menu item to install different software 

--- 

## Arch Script

1. [Sub-Menu] Add AUR Support 
    - Install yay option
    - Install trizen option
2. [Submenu] Manjaro Specific
3. Add Sound Support
    - Adds sound support 
4. Add Archive Support
    - Adds support for file archives 



## Fedora Script

1. Add Fedy
2. Perform Common Fedy Tasks (all these tasks at once)

    - Fedy Tasks
      1. Install Archive Support
      2. Install Microsoft Fonts
      3. Install Codecs
      4. Install Theme Engines
      5. Improve Font Rendering

## OpenSuse Script

1. Add Cinnamon Repo
2. Add Plasma 5 Repos

## Solus Scripts 

1.  Switch from Stable Repo to Unstable Repo
2.  Switch from Unstable Repo to Stable Repo

### Individual Tasks

3. Add Unstable Repo (DOES NOT disable the default repo or ENABLE Unstable repos)

4. Disable Unstable Repo (useful for when the Solus Team says "Dont Update, if you're using the Unstable Repo")
   - (note to self: What is the difference between this and #2? .... this seems redundent)


## Ubuntu Script

Removed most content now that things are moving to Universal functions 

- Different tasks like "automatically setting up a number of tasks, post ElementaryOS install, will likely go here

--- 
## Theming Scripts 

1. Install Adapta Theme
2. Install Arc Theme
3. Install Mint-Y Theme
4. Install Numix Theme
5. Install Plata Theme
